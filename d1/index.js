// Synchronous and Asynchronous

	// Javascript is by default synchronous meaning it only runs one statement at a time.

	// Javascript is single-threaded.

	// TASK A -> TASK B -> TASK C


// console.log('Hello World');

// console.log('Hello Again');

// for(let i = 0; i <= 1500; i++){
// 	console.log(i);
// }
// console.log('The End');

	// When an action will take some time to process, this results in code "blocking".

// Asynchorous
	// means that we can proceed to  execute other statments, while time consuming codes is running in the background.

// Getting all posts
	/*
		Syntax:
			fetch('URL')
			console.log(fetch())

	*/

	// fetch('https://jsonplaceholder.typicode.com/posts')
	// fetch allows you to asynchronooulsy requst or fetch data
			// console.log(fetch('https://jsonplaceholder.typicode.com/posts'))
/*

	Syntax:
	fetch('URL')
	.then((response) => ())

*/

// fetch('https://jsonplaceholder.typicode.com/posts')
// 	.then((response) => console.log(response.status))

	// .then captures the response object and return another promise which will eventually be  resolved or rejected.

// fetch('https://jsonplaceholder.typicode.com/posts')
// 	.then((response) => response.json(response.status))

	// json method from response object to convert the data retrieved into JSON format to be used in our application.

	// .then((json) => console.log(json))

	// print the converted JSON value from the fetch request.

// Using multiple ".then" methods created a "promise chain"


// Async - Await

// async function fetchData(){
// 	let result = await fetch('https://jsonplaceholder.typicode.com/posts')
// 	console.log(result);
// 	console.log(typeof result);
// 	console.log(result.body);

// 	let json = await result.json()
// 	console.log(json);
// };

// fetchData();

// Synatatic Sugar
	// makes the code more readable.
	// but not necessarily better.

// Async-Await is the syntatic sugar of .then method.

	// Retrieve specific post
	// fetch('https://jsonplaceholder.typicode.com/posts/14')
	// .then((response) => response.json())
	// .then((json)  => console.log(json))

// Creating a post

	// Syntax:
		// fetch ('URL', options)
		// .then ((response) => {})
		// .then((response) => {})

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			title: 'New Post',
			body: 'Hello World',
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))

// update a post

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			id: 1,
			title: 'Updated Post',
			body: 'Hellow Once More',
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log (json))

// Updating post with PATCH
	
	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PATCH',
		headers: {
			'Content-Type' : 'application/json'
		},
		body:JSON.stringify({
			title: 'Corrected Post',
			body: 'Corrected content'
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))

// PUT and PATCH
	// both deals with updates
	// The number of properties being changed.

// Deleting a post

	// fetch('https://jsonplaceholder.typicode.com/posts/1', {
	// 	method: 'DELETE'
	// })

	fetch('https://jsonplaceholder.typicode.com/posts?userId=1', {
		method: 'PUT',
		headers:{
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			id: 1,
			title: 'Updated Post Again',
			body: 'Hellow Once More Once More',
			userId: 1
	})
	.then((response) => response.json())
	.then((json) => console.log(json))

	// Data can be filtered by sending the userid along with the URL
	// Information sent via the URL can be done by adding question mark (?).